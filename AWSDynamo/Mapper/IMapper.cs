﻿using Amazon.DynamoDBv2.DataModel;
using AWSDynamo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWSDynamo.Mapper
{
    public interface IMapper
    {
        IEnumerable<DeviceResponse> ToDeviceContract(IEnumerable<DeviceDb> items);

        DeviceResponse ToDeviceContract(DeviceDb device);

        DeviceDb ToDeviceDbModel(CreateDeviceRequest request);

        DeviceDb ToDeviceResponseDbModel(int userId, DeviceDb deviceDbRequest, DeviceResponse deviceUpdateRequest);
    }

    public class Mapper : IMapper
    {
        public IEnumerable<DeviceResponse> ToDeviceContract(IEnumerable<DeviceDb> items)
        {
            return items.Select(ToDeviceContract);
        }

        public DeviceResponse ToDeviceContract(DeviceDb device)
        {
            return new DeviceResponse
            {
                 AuthCode = device.AuthCode
            };
        }

        public DeviceDb ToDeviceDbModel(CreateDeviceRequest request)
        {
            return new DeviceDb
            {
                UserId = request.UserId,
                DeviceId = request.DeviceId,
                AuthCode = request.AuthKey,
                RegistrationDate = request.RegistrationDate,
                UnregistraionDate =request.UnregistrationDate
                
            };
        }

        public DeviceDb ToDeviceResponseDbModel(int userId, DeviceDb deviceDbRequest, DeviceResponse deviceUpdateRequest)
        {
            return new DeviceDb
            {
                UserId = deviceDbRequest.UserId,
         
            };
        }
    }

    [DynamoDBTable("DeviceManagementIntegrationTest")]
    public class DeviceDb
    {
        [DynamoDBHashKey]
        public string UserId { get; set; }

        [DynamoDBGlobalSecondaryIndexHashKey]
        public string DeviceId { get; set; }

        public string AuthCode { get; set; }
        public string RegistrationDate { get; internal set; }
        public string UnregistraionDate { get; internal set; }

    }

    public class DeviceResponse
    {
        public string AuthCode { get; set; }

    }

    public class DeviceRequest
    {
        public string UserId { get; set; }
    }

    public class DeviceUpdateRequest
    {
        public string DeviceId { get; set; }
        public string AuthCode { get; set; }
    }
}
