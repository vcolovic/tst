﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.Model;
using AWSDynamo.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWSDynamo.Repository
{
    public interface IDeviceRepository
    {
        Task AddDevice(DeviceDb device);
    }
    public class DeviceRepository : IDeviceRepository
    {
        private readonly IDynamoDBContext context;
       
        public DeviceRepository(IDynamoDBContext context)
        {
           this.context = context;
        }
        public async Task AddDevice(DeviceDb device)
        {
            await context.SaveAsync(device);
        }

      
    }
}
