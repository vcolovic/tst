﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AWSDynamo.Repository
{
    public interface IAmazonStoreRepo
    {
        Task TearDownStore(string storeName);
        Task TryCreateStore(string storeName);
        Task<bool> StoreExists(string storeName);
        Task WaitUntilStoreActive(string storeName);
        Task<string> GetStoreStatus(string storeName);
    }
    public class AmazonStoreRepo : IAmazonStoreRepo
    {
        private  readonly IAmazonDynamoDB dynamoDbClient;

        public AmazonStoreRepo(IAmazonDynamoDB dynamoDbClient)
        {
            this.dynamoDbClient = dynamoDbClient;
        }
        public  async Task TearDownStore(string storeName)
        {
            var request = new DeleteTableRequest
            {
                TableName = storeName
            };

            await dynamoDbClient.DeleteTableAsync(request);
        }
        public async Task TryCreateStore(string storeName)
        {
           
            var request = new CreateTableRequest
            {
                AttributeDefinitions = new List<AttributeDefinition>()
                {
                    new AttributeDefinition
                    {
                        AttributeName = "UserId",
                        AttributeType = "S"
                    },
                    new AttributeDefinition
                    {
                        AttributeName = "DeviceId",
                        AttributeType = "S"
                    }
                },
                KeySchema = new List<KeySchemaElement>
                {
                    new KeySchemaElement
                    {
                        AttributeName = "UserId",
                        KeyType = "HASH"
                    },
                    new KeySchemaElement
                    {
                        AttributeName = "DeviceId",
                        KeyType = "RANGE"
                    }
                },
                ProvisionedThroughput = new ProvisionedThroughput
                {
                    ReadCapacityUnits = 1,
                    WriteCapacityUnits = 1
                },
                TableName = storeName,

                GlobalSecondaryIndexes = new List<GlobalSecondaryIndex>
                {
                    new GlobalSecondaryIndex
                    {
                        IndexName = "DeviceId-index",
                        KeySchema = new List<KeySchemaElement>
                        {
                            new KeySchemaElement
                            {
                                AttributeName = "DeviceId",
                                KeyType = "HASH"
                            }
                        },
                        ProvisionedThroughput = new ProvisionedThroughput
                        {
                            ReadCapacityUnits = 1,
                            WriteCapacityUnits = 1
                        },
                        Projection = new Projection
                        {
                            ProjectionType = "ALL"
                        }
                    }
                }
            };
          
            var n = await StoreExists(storeName).ContinueWith(async r=> {
                if (r.Result)
                {
                    await dynamoDbClient.CreateTableAsync(request);
                }
            });

          


        }

        public async Task<bool> StoreExists(string storeName)
        {
           
         return await dynamoDbClient.DescribeTableAsync(new DescribeTableRequest
            { TableName = storeName }).ContinueWith( r =>
            {
                return  r.IsFaulted;
            });
        }

        public  async Task WaitUntilStoreActive(string storeName)
        {
            string status = null;
            do
            {
                Thread.Sleep(5000);
                try
                {
                    status = await GetStoreStatus(storeName);
                }
                catch (ResourceNotFoundException)
                {

                    throw;
                }
            }
            while (status != "ACTIVE");
        }

        public async Task<string> GetStoreStatus(string storeName)
        {
            var response = await dynamoDbClient.DescribeTableAsync(new DescribeTableRequest
            {
                TableName = storeName
            });
            return response.Table.TableStatus;
        }
      
    }
}
