﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using AWSDynamo.Mapper;
using AWSDynamo.Repository;
using AWSDynamo.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AWSDynamo
{
    public class Startup
    {
        private readonly IHostingEnvironment _env;
        public IConfiguration Configuration { get; }
        
        public Startup(IConfiguration configuration, IHostingEnvironment env)

        {
            Configuration = configuration;
            _env = env;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            if (_env.IsDevelopment())
            {

                var clientConfig = new AmazonDynamoDBConfig { UseHttp = true, ServiceURL = "http://localhost:8000"};
                    services.AddSingleton<IAmazonDynamoDB>(sp => new AmazonDynamoDBClient(clientConfig));
                services.AddSingleton<IDynamoDBContext>(sp => new DynamoDBContext(sp.GetService<IAmazonDynamoDB>()));
            }
            else
            {
                AmazonDynamoDBConfig config = new AmazonDynamoDBConfig { RegionEndpoint = RegionEndpoint.USEast2 };
                services.AddSingleton<IAmazonDynamoDB>(sp => new AmazonDynamoDBClient(config));
                services.AddSingleton<IDynamoDBContext>(sp => new DynamoDBContext(sp.GetService<IAmazonDynamoDB>()));
            }

            services.AddMvc();
            services.AddSingleton<IDeviceService, DeviceService>();
            services.AddSingleton<IDeviceRepository, DeviceRepository>();
            services.AddSingleton<IAmazonStoreRepo, AmazonStoreRepo>();
            services.AddSingleton<IMapper, Mapper.Mapper>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseMvc();
        }
    }
}
