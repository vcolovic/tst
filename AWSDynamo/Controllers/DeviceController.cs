﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using AWSDynamo.Mapper;
using AWSDynamo.Models;
using AWSDynamo.Services;
using Microsoft.AspNetCore.Mvc;

namespace AWSDynamo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeviceController : Controller
    {
        private IDeviceService deviceService;

        public DeviceController(IDeviceService device)
        {
            this.deviceService = device;
        }



        [HttpPost]
        public async Task<IActionResult> AddDevice(CreateDeviceRequest createDeviceRequest)
        {
            await deviceService.AddDevice(createDeviceRequest);
            
            return Ok();
        }
    }
}