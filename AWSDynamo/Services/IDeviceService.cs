﻿using AWSDynamo.Mapper;
using AWSDynamo.Models;
using AWSDynamo.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWSDynamo.Services
{
    public interface IDeviceService
    {
        Task AddDevice(CreateDeviceRequest request);
    }

    public class DeviceService : IDeviceService
    {
        private readonly IDeviceRepository repo;
        private readonly IMapper _map;
        public DeviceService(IDeviceRepository repo, IMapper map)
        {
            this.repo = repo;
            _map = map;
           
        }
      
        public async Task AddDevice(CreateDeviceRequest request)
        {
           //  await repo.CreateTable("DeviceManager1988");
            var model = _map.ToDeviceDbModel(request);
            await repo.AddDevice(model);
        }
    }
}
