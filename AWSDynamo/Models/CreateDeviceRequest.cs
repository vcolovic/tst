﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWSDynamo.Models
{
    public class CreateDeviceRequest
    {
        public string DeviceId { get; set; }
        public string UserId { get; set; }
        public string AuthKey { get; set; }
        public string RegistrationDate { get; set; }
        public string UnregistrationDate { get; set; }
    }
}
