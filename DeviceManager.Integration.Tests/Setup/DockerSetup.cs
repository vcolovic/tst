﻿using Docker.DotNet;
using Docker.DotNet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace DeviceManager.Integration.Tests.Setup
{
    public class DockerSetup /*: IDisposable*/
    {
        private readonly DockerClient _dockerClient;

        private string _containerId;
        private const string ContainerImageUri = "instructure/dynamo-local-admin";

        public DockerSetup()
        {
            _dockerClient = new DockerClientConfiguration(new Uri(DockerApiUri())).CreateClient();
        }

        public async Task InitializeAsync()
        {
            await PullImage();
            await StartContainer();
        }

        private string DockerApiUri()
        {
            var isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);

            if (isWindows)
            {
                return "npipe://./pipe/docker_engine";
            }

            var isLinux = RuntimeInformation.IsOSPlatform(OSPlatform.Linux);

            if (isLinux)
            {
                return "unix:/var/run/docker.sock";
            }

            throw new Exception(
                "Was unable to determine what OS this is running on, does not appear to be Windows or Linux!?");
        }

        private async Task PullImage()
        {
            await _dockerClient.Images
                .CreateImageAsync(new ImagesCreateParameters
                {
                    FromImage = ContainerImageUri,
                    Tag = "latest"
                },
                    new AuthConfig(),
                    new Progress<JSONMessage>());
        }

        private async Task StartContainer()
        {
            await _dockerClient.Containers.ListContainersAsync(new ContainersListParameters { All = true }).ContinueWith( c =>
            {
                var containers = c.Result;
                var id = containers.Where(n => n.Image == ContainerImageUri).FirstOrDefault();
                _dockerClient.Containers.KillContainerAsync(id.ID, new ContainerKillParameters());
            }).ContinueWith( s =>
            {
               var response = _dockerClient.Containers.CreateContainerAsync(new CreateContainerParameters
                {
                    Image = ContainerImageUri,
                    ExposedPorts = new Dictionary<string, EmptyStruct>
                {
                    {
                        "8000", default(EmptyStruct)
                    }
                },
                    HostConfig = new HostConfig
                    {
                        PortBindings = new Dictionary<string, IList<PortBinding>>
                    {
                        {"8000", new List<PortBinding> {new PortBinding {HostPort = "8000"}}}
                    },
                        PublishAllPorts = true
                    }
                });

                _containerId = response.Result.ID;
            });

            await _dockerClient.Containers.StartContainerAsync(_containerId, null);
        }

        //public  void Dispose()
        //{
        //    if (_containerId != null)
        //    {
        //        await _dockerClient.Containers.KillContainerAsync(_containerId, new ContainerKillParameters());
        //    }
        //}
    }
}
