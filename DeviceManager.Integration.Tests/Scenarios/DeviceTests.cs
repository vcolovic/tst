﻿using Amazon.DynamoDBv2;
using AWSDynamo;

using AWSDynamo.Models;
using AWSDynamo.Repository;
using DeviceManager.Integration.Tests.Setup;
using FluentAssertions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DeviceManager.Integration.Tests.Scenarios
{
    [TestFixture]
    public class DeviceTests
    {
        private DockerSetup docker;
        private AmazonStoreRepo amazonRepo;
        private TestServer _server;
        public HttpClient Client { get; set; }
        private static readonly IAmazonDynamoDB DynamoDBClient = new AmazonDynamoDBClient(new AmazonDynamoDBConfig
        {
            ServiceURL = "http://localhost:8000"
        });

        [SetUp]
        public async Task BaseSetUp()
        {

            docker = new DockerSetup();
            await docker.InitializeAsync();
          
             amazonRepo = new AmazonStoreRepo(DynamoDBClient);
             await amazonRepo.TryCreateStore("DeviceManagementIntegrationTest");
             await amazonRepo.WaitUntilStoreActive("DeviceManagementIntegrationTest");
            _server = new TestServer(new WebHostBuilder()
              .UseEnvironment("Development")
              .UseStartup<Startup>());
            _server.BaseAddress = new Uri("http://localhost:5000");

            Client = _server.CreateClient();
            Client.BaseAddress = new Uri("http://localhost:6000");

            //   amazonRepo.TryCreateStore("vladaTest").Wait();
        }
        [Test]
        public async Task AddDeviceToDynamo()
        {
            Random r = new Random();
            CreateDeviceRequest createRequest = new CreateDeviceRequest
            {
                UserId = Guid.NewGuid().ToString(),
                DeviceId = r.Next(1, 1000).ToString(),
                AuthKey = Guid.NewGuid().ToString(),
                RegistrationDate = DateTime.Now.ToString(),
                UnregistrationDate = DateTime.Now.AddDays(10).ToString()
            };
            var content = JsonConvert.SerializeObject(createRequest);
            var stringContent = new StringContent(content, Encoding.UTF8, "application/json");
            var reponse = await Client.PostAsync($"api/device", stringContent);
            reponse.StatusCode.Should().Be(HttpStatusCode.OK);
        }
    }
}
